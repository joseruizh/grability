<?php



Class Refactoring {
    
    public function post_confirm() {
        $id = Input::get('service_id');
        $servicio = Service::find(id);

        //El servicio debe existir
        if($servicio == NULL) {
            return Response::json(['error' => '3']);
        }

        //El id del status del servicio debe ser distinto de 6
        if($servicio->status_id == '6') {
            return Response::json(['error' => '2']);
        }

        //El servicio no debe tener conductor asignado y su id de status debe ser igual a 1
        if($servicio->driver_id != NULL || $servicio->status_id != '1') {
            return Response::json(['error' => '1']);
        }


        $driver_id = Input::get('driver_id');
        $driver = Driver::find($driver_id);

        //El conductor debe existir
        if($driver != NULL) {
            return Response::json(['error' => 'Asignar un código de error para este tipo de problema.']);
        }

        //Asignamos car_id en la misma transacción que hacemos la del conductor, ya que la actualización de car_id no esta condicionada
        Service::update($id, [
            'driver_id' => $driver_id,
            'status_id' => '2',
            'car_id' => $driver->car_id
        ]);

        //Marcamos el conductor como no disponible
        Driver::update($driver_id, ['available' => '0']);

        $uuid = $servicio->user->uuid;        
        //Hacemos la notificación Push si el usuario tiene asignado un uuid
        if(!empty($uuid)) {
            //Notificar usuario
            $pushMessage = 'Tu servicio ha sido confirmado!';
            //Delegamos la selección del tipo de notificación push a un metodo destinado para esto
            $this->send_push_notification($uuid, $pushMessage, '1', 'Open', ['serviceId' => $servicio->id], $servicio->user->type);
        }

        return Response::json(['error' => '0']);
    }

    private function send_push_notification($uuid, $pushMessage, $flag, $status, $payload, $serviceUserType) {

        $push = Push::make();

        if($serviceUserType == '1') {
            $push->ios($uuid, $pushMessage, $flag, 'honk.wav', $status, $payload);
        } else {
            $push->android2($uuid, $pushMessage, $flag, 'default', $status, $payload);
        }
    }
}
